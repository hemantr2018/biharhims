import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  switchLoginTab(){
    this.router.navigate(['/login']);
  }

  proceed(){
    console.log('entered')
    this.router.navigate(['/roleRegister'])
  }
}
