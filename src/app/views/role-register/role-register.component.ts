import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-register',
  templateUrl: './role-register.component.html',
  styleUrls: ['./role-register.component.css']
})
export class RoleRegisterComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  switchLoginTab(){
    this.router.navigate(['/login']);
  }
}
